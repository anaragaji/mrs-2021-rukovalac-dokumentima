from PySide2 import QtCore, QtWidgets, QtGui


class AboutBox(QtWidgets.QDialog):
    def __init__(self, parent):
        super().__init__(parent)
        self.resize(350, 250)
        self.setWindowTitle("O aplikaciji")
        self.setWindowIcon(QtGui.QIcon("resources/icons/i.png"))
        self.button_box = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Close, parent=self )
        self.stacked = QtWidgets.QStackedWidget(self)
        self.manager_layout = QtWidgets.QVBoxLayout()
        
        self.manager_layout.stretch(3)
        self.label = QtWidgets.QLabel("Aplikacija koja sluzi kao alat za upravljanje razlicitih tipova dokumenata. \n\n Trenutna verzija: 1.1.2")
    
        self.label.setWordWrap(True)
        self.label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        
        self.manager_layout.addWidget(self.label)
        
        self.manager_layout.addWidget(self.button_box, alignment=QtCore.Qt.AlignRight)
        
        self.setLayout(self.manager_layout)
        

        self.button_box.rejected.connect(self.reject)