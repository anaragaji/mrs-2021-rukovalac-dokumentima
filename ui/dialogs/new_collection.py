from tkinter import CENTER
from PySide2 import QtCore, QtGui, QtWidgets
import sys
import os
class AnotherWindow(QtWidgets.QWidget):

    def __init__(self):
        super().__init__()
        # layout = QtWidgets.QVBoxLayout()
        # self.label = QtWidgets.QLabel("Another Window")
        # layout.addWidget(self.label)
        # self.setLayout(layout)
        # self.initUI()
    

        QtWidgets.QToolTip.setFont(QtGui.QFont('SansSerif', 10))

        self.path = ".\docs"
      
        labelProjectName = QtWidgets.QLabel('Collection Name: ', self)
        labelProjectName.move(15, 15)

        self.etProjectName = QtWidgets.QLineEdit('', self)
        self.etProjectName.resize(self.etProjectName.sizeHint())
        self.etProjectName.move(130, 7)

        self.btn = QtWidgets.QPushButton('Create Collection', self)
        self.btn.setToolTip('This creates the folders.')
        self.btn.resize(self.btn.sizeHint())
        self.btn.move(113, 80)    
        
           
        self.btn.clicked.connect(self.generateFolders)
        
        self.resize(350, 150)
       

        self.setWindowTitle('New Collection')    
        

    def uniquify(self, path):
        filename, extension = os.path.splitext(path)
        counter = 1

        while os.path.exists(path):
            path = filename + " (" + str(counter) + ")" + extension
            counter += 1

        return path


    def generateFolders(self):
        
        directory = self.path
        self.projectName = self.etProjectName.text()
        if self.projectName.__eq__(''):
            self.projectName = "New Collection"
        self.filePath = str(directory) +  "/" + str(self.projectName) 
        
            
        nov = self.uniquify(self.filePath)
        os.makedirs(nov)
        self.etProjectName.clear()
        self.close()






