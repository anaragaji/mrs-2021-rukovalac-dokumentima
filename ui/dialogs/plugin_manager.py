import sys
sys.path.append("...")
from PySide2 import QtGui, QtWidgets
from model.table import TableModel
from plugin_framework.plugin_registry import PluginRegistry



class PluginManager(QtWidgets.QDialog):
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.setWindowTitle("Plugin Manager")
        self.setWindowIcon(QtGui.QIcon("resources/icons/plugin.png"))
        self.resize(920, 240)
        # self.resize(self.table_view.width(), 200)
        self.button_box = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Save | QtWidgets.QDialogButtonBox.Close, parent=self )
        self.grp = QtWidgets.QGroupBox("Check for plugin activation")
        self.button_box.accepted.connect(self.save_state)
        self.manager_layout = QtWidgets.QVBoxLayout()
        self.plugin_registry = PluginRegistry("plugins", self.parent)
        
        self.button_box.rejected.connect(self.reject)
        
        # self.plugin_registry.activate(222222222)
        self.data_list = []
        
        self.komponente = self.plugin_registry._plugins
        for komponenta in self.komponente:
            #Svakoj komponenti se dodeljuje checkbox
            self.arr = [QtWidgets.QCheckBox(""), komponenta.plugin_specification.name, komponenta.plugin_specification.id, komponenta.plugin_specification.category, komponenta.plugin_specification.description, "Disabled"]
            if komponenta.active:
                self.plugin_registry.activate(komponenta.plugin_specification.id)
                self.arr[0].setChecked(True)
                self.arr[5] = "Enabled"
            self.data_list.append(self.arr) 
            


        self.header = ['', 'Plugin name', ' ID', ' Category', 'Description' , ' Working']

        self.model = TableModel(self, self.data_list, self.header)
        self.table_view = QtWidgets.QTableView()

        self.table_view.setModel(self.model)
        self.table_view.setColumnHidden(2, True) # Odluka da se ne prikazuje kolona sa identifikatorima
        self.table_view.resizeColumnsToContents()



        self.manager_layout.stretch(5)
        self.manager_layout.addWidget(self.table_view)



        self.manager_layout.addWidget(self.button_box)
        self.setLayout(self.manager_layout)
    
        

        
    def save_state(self):  
        for i in self.komponente:
            self.de()
        
        self.act()


    def act(self):
        
        for row in self.model.mylist:
            
            if row[0].isChecked():
                self.plugin_registry.activate(row[2]) #row[2] = ID
                row[5] = "Enabled"

                


    def de(self):
        for row in self.model.mylist:
            
            if not row[0].isChecked():
                self.plugin_registry.deactivate(row[2]) #row[2] = ID
                row[5] = "Disabled" 
                
            
            





        





                
                

