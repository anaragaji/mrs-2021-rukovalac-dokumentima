from PySide2 import QtCore, QtGui, QtWidgets
import sys
import os
class NewFile(QtWidgets.QWidget):

    def __init__(self):
        super().__init__()

    

        QtWidgets.QToolTip.setFont(QtGui.QFont('SansSerif', 10))

        self.path = ".\docs"
        self.item = ""
      
        labelProjectName = QtWidgets.QLabel('File Name: ', self)
        labelProjectName.move(15, 15)

        self.etProjectName = QtWidgets.QLineEdit('', self)
        self.etProjectName.resize(self.etProjectName.sizeHint())
        self.etProjectName.move(130, 7)

        self.btn = QtWidgets.QPushButton('Create File', self)
        self.btn.setToolTip('This creates the files.')
        self.btn.resize(self.btn.sizeHint())
        self.btn.move(113, 80)    
        
           
        self.btn.clicked.connect(self.generateFiles)
        
        self.resize(350, 150)
       

        self.setWindowTitle('New File')    
        

    def uniquify(self, path):
        filename, extension = os.path.splitext(path)
        counter = 1

        while os.path.exists(path):
            path = filename + " (" + str(counter) + ")" + extension
            counter += 1

        return path


    def generateFiles(self):
        
        directory = self.path
        self.projectName = self.etProjectName.text()
        if self.projectName.__eq__(''):
            self.projectName = "New File"
        self.filePath = str(directory) +  "/" + str(self.projectName)  + ".docf"
        
            
        nov = self.uniquify(self.filePath)
        # os.makedirs(nov)
        self.etProjectName.clear()
        self.close()
        return self.item.newFile(nov)







