from PySide2 import QtWidgets, QtCore, QtGui

class CentralWidget(QtWidgets.QTabWidget):
    def __init__(self, title, parent):
        super().__init__(parent)

        self.parent = parent
        self.index = 0
        self.scroll = QtWidgets.QScrollArea()
        self.starting = QtWidgets.QWidget(self)
        self.scroll.work_type = "None"
        # self.setStyleSheet("background-color: grey;")
        
        
        # self.scroll.setWidget(self.statring)
        self.label = QtWidgets.QLabel()
        self.slika = QtGui.QPixmap("resources/icons/project.png")
        # self.slika.setSize(5)
        
        self.label.setPixmap(self.slika)
        
        self.label2 = QtWidgets.QLabel("Dobrodošli u aplikaciju Rukovalac Dokumentima!\n\nUkoliko želite početi sa radom odaberite odgovarajuću komponentu\n (Plugins > Plugin Manager)\nZa vise informacija pogledajte About")
        self.lay = QtWidgets.QVBoxLayout()
        
        self.lay.addWidget(self.label)
        self.lay.addWidget(self.label2)
        # self.lay.addWidget(self.scroll)
        
        self.label2.setWordWrap(True)
        self.label2.setFont(QtGui.QFont('Arial', 15))
        self.label2.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.starting.setLayout(self.lay)
        self.scroll.setWidget(self.starting)
        # self.starting.setLayout(self.scroll)
        self.addTab(self.scroll, "Starting")
        
        
        

        
    def delete_tab(self):
        self.removeTab(self.index)

        

    def add_widget(self, widget):

        self.addTab(widget, widget.icon, widget.name)
        


        



