from PySide2 import QtWidgets, QtCore, QtGui
from ui.dialogs.about import AboutBox

class ToolBar(QtWidgets.QToolBar):
    def __init__(self, title, parent):
        super().__init__(parent)
        self.parent = parent
        self.about_box = AboutBox(self)

        self._populate_tool_bar()
        
        self.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
        
    

    def _populate_tool_bar(self):
        self.addAction(self.parent.actions_dict["new_collection"])
        # self.addAction(self.parent.actions_dict["open_file"])
        self.addAction(self.parent.actions_dict["new_file"])
        # self.addAction(self.parent.actions_dict["save"])
        # self.addAction(self.parent.actions_dict["save_as"])
        # self.addAction(self.parent.actions_dict["undo"])
        self.addAction(self.parent.actions_dict["about"])
        self.addAction(self.parent.actions_dict["add_new_text_box"])
        
        