from PySide2 import QtCore, QtWidgets, QtGui

from plugins.workspace.widget import StructureDock
from .dialogs.plugin_manager import PluginManager

from .utils.central_widget import CentralWidget
from .utils.tool_bar import ToolBar
from ui.dialogs.about import AboutBox



class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, title, icon, parent=None):
        super().__init__(parent)
        self.setWindowTitle(title)
        self.setWindowIcon(icon)
        self.resize(950, 900)
        self.status_bar = QtWidgets.QStatusBar(self)
        self.setStatusBar(self.status_bar)
        self.status_bar.showMessage("Loaded, waiting for action")
        # self.sd = StructureDock("title", self)
        


        # self.sd = StructureDock("Dock", self)
        # self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.sd)

        # self.sd.klik.connect(self.open_f)    
        # self.sd.klik.connect(self.newFile)   


        self.actions_dict = {
            
            "new_file": QtWidgets.QAction(QtGui.QIcon("resources/icons/new_file.png"),"&New File", self),
            "new_collection" : QtWidgets.QAction(QtGui.QIcon("resources/icons/New-Folder-icon.png"),"&New Collection", self),
            "open_file":QtWidgets.QAction(QtGui.QIcon("resources/icons/open.png"), "&Open File", self),
            "open_recent":QtWidgets.QAction("&Open Recent", self),
            "save": QtWidgets.QAction(QtGui.QIcon("resources/icons/save.png"), "&Save", self),
            "save_as": QtWidgets.QAction(QtGui.QIcon("resources/icons/save-as.png"), "&Save As", self),
            "quit": QtWidgets.QAction(QtGui.QIcon("resources/icons/close.png"), "&Quit", self),
            "undo": QtWidgets.QAction(QtGui.QIcon("resources/icons/undo.png"), "&Undo", self),
            "redo":QtWidgets.QAction("&Redo", self),
            "copy":QtWidgets.QAction("&Copy", self),
            "paste":QtWidgets.QAction("&Paste", self),
            "zoom":QtWidgets.QAction("&Zoom", self),
            "about": QtWidgets.QAction(QtGui.QIcon("resources/icons/i.png"), "&About", self),
            "plugin_manager" : QtWidgets.QAction(QtGui.QIcon("resources/icons/plugin.png"), "&Plugin Manager", self),
            "add_new_text_box" : QtWidgets.QAction(QtGui.QIcon("resources/icons/new_text.png"), "&New Text Box", self)
        }

        
        self._populate_main_window()
        


    def _populate_menu_bar(self):
        file_menu = QtWidgets.QMenu("&File")
        edit_menu = QtWidgets.QMenu("&Edit")
        view_menu = QtWidgets.QMenu("&View")
        plugins_menu = QtWidgets.QMenu("&Plugins")
        help_menu = QtWidgets.QMenu("&Help")

        
        file_menu.addAction(self.actions_dict["new_file"])
        file_menu.addAction(self.actions_dict["open_file"])
        file_menu.addAction(self.actions_dict["open_recent"])
        file_menu.addSeparator()
        file_menu.addAction(self.actions_dict["save"])
        file_menu.addAction(self.actions_dict["save_as"])
        file_menu.addSeparator()
        file_menu.addAction(self.actions_dict["quit"])

        edit_menu.addAction(self.actions_dict["undo"])
        edit_menu.addAction(self.actions_dict["redo"])
        edit_menu.addSeparator()
        edit_menu.addAction(self.actions_dict["copy"])
        edit_menu.addAction(self.actions_dict["paste"])

        view_menu.addAction(self.actions_dict["zoom"])

        plugins_menu.addAction(self.actions_dict["plugin_manager"])

        self.menu_bar.addMenu(file_menu)
        self.menu_bar.addMenu(edit_menu)
        self.menu_bar.addMenu(view_menu)
        self.menu_bar.addMenu(plugins_menu)
        self.menu_bar.addMenu(help_menu)


    def _bind_actions(self):
        self.actions_dict["quit"].setShortcut("Ctrl+Q")
        self.actions_dict["quit"].triggered.connect(self.close)
        self.actions_dict["save"].setShortcut("Ctrl+S")
        # self.actions_dict["save"].triggered.connect(self.save_f)
        # self.actions_dict["save_as"].triggered.connect(self.save_as)
        self.actions_dict["undo"].setShortcut("Ctrl+Z")
        self.actions_dict["redo"].setShortcut("Ctrl+Y")
        self.actions_dict["copy"].setShortcut("Ctrl+C")
        self.actions_dict["paste"].setShortcut("Ctrl+V")
        self.actions_dict["plugin_manager"].setShortcut("Ctrl+F")
        self.actions_dict["plugin_manager"].triggered.connect(self.pl.show)
        
        # self.actions_dict["open_file"].triggered.connect(self.open)
        self.actions_dict["about"].triggered.connect(self.about_box.show)
        
        # self.actions_dict["new_collection"].triggered.connect(self.sd.pr)
        
    

    def _populate_main_window(self):
        # dock widget i cw premesteni u posebne klase.

        # self.sd = StructureDock("Dock", self)
        # self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.sd)

        # self.sd.klik.connect(self.open_f)   

        self.central_widget = CentralWidget("Central widget", self)
        self.setCentralWidget(self.central_widget)
        
        self.about_box = AboutBox(self)
        self.pl = PluginManager(self)
        

        # self.sd = StructureDock("Dock", self)
        # self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.sd)

        self.menu_bar = QtWidgets.QMenuBar(self)

        self.tool_bar = ToolBar("title", self)
        self.addToolBar(self.tool_bar)

        
        self._bind_actions()

        self._populate_menu_bar()


        self.setMenuBar(self.menu_bar)
