import sys
from PySide2 import QtWidgets, QtGui
from ui.main_window import MainWindow


if __name__ == "__main__":

    application = QtWidgets.QApplication(sys.argv)

    main_window = MainWindow("Rukovalac Dokumentima", QtGui.QIcon("resources/icons/project.png"))


    main_window.show()

    sys.exit(application.exec_())