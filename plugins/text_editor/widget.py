from asyncio.windows_events import NULL
from turtle import setpos, setposition
from PySide2 import QtGui, QtWidgets, QtCore


class TextBox(QtWidgets.QGraphicsTextItem):
    widget_for = 123456789
    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        
        self.scene = NULL


    def new_text_box(self, scene):


        # self.setZValue(-4)

        self.setFlag(QtWidgets.QGraphicsTextItem.ItemIsMovable)
        self.setFlag(QtWidgets.QGraphicsTextItem.ItemIsFocusable)
        self.setFlag(QtWidgets.QGraphicsTextItem.ItemIsSelectable)
        self.setTextInteractionFlags(QtCore.Qt.TextEditorInteraction)
        self.setFont("Sanserif")
        self.setScale(2)
        
        self.setPlainText("New text box")
        
        scene.addItem(self)

        




        


    
