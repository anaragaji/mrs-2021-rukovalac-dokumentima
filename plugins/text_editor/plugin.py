
from plugin_framework.extension import Extension
from .widget import TextBox
from PySide2 import QtGui, QtWidgets


class Plugin(Extension):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)


        self.widget = TextBox(iface.central_widget)
        self.widget.work_type = "Tekst"
        self.parent = self.iface.central_widget
        # self.index = self.widget.currentIndex()
        self.active = False

        
        
        print("INIT TEST")

    # FIXME: implementacija apstraktnih metoda
    def activate(self):
        # print("Activated TextEdit")
        
        self.active = True
        nesto = self.iface.central_widget.currentWidget().page.scene
        self.iface.actions_dict["add_new_text_box"].triggered.connect(self.widget.new_text_box(nesto))
        
        # self.iface.status_bar.showMessage("Activated " + self.widget.name)
        
        

    def deactivate(self):
        # print("Deactivated TextEdit")
        self.active = False
        # self.widget.clear()
        # self.iface.central_widget.delete_tab()
        