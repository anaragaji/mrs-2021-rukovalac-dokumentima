
from PySide2 import QtWidgets
from plugin_framework.extension import Extension

from .widget import DocumentHandler

class Plugin(Extension):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)
        
        



        self.widget = DocumentHandler(iface.central_widget)

        self.widget.work_type = "handler"
        
        self.parent = self.iface.central_widget

        

        # self.setLayout(self.layout)
        # self.index = self.widget.currentIndex()
        self.active = False
        
        
        print("INIT TEST")

    # FIXME: implementacija apstraktnih metoda
    def activate(self):
        # print("Activated Mock")
        
        self.active = True
        self.iface.central_widget.add_widget(self.widget)
        
        
        
        # self.iface.status_bar.showMessage("Activated " + self.widget.name)
        
        

    def deactivate(self):
        # print("Deactivated Mock")
        self.active = False
        self.widget.clear()
        self.iface.central_widget.delete_tab()