from operator import index
from PySide2 import QtCore, QtGui, QtWidgets
from .page import Page



class DocumentHandler(QtWidgets.QTabWidget):
    widget_for = 111111111
    def __init__(self, parent):
        super().__init__(parent)
        self.setTabsClosable(True)
        self.tabCloseRequested.connect(self.delete_tab) ### ??
        self.parent = parent
        self.index = 0
        self.name = "    Document Handler   "
        self.icon = QtGui.QIcon("resources/icons/edit.png")
        self.tabNames = []
        self.tab = 0
        self.page = 0




        
        


        
    def delete_tab(self, index):
        
        qm = QtWidgets.QMessageBox()
        rez = qm.question(self,'', "Da li sigurno zelite zatvoriti trenutnu stranicu?", qm.Yes | qm.No)

        if rez == qm.Yes:
            self.removeTab(index)
        else:
            pass
        
        

    def dodaj(self, name, txt):
        
        if self.tabNames.__contains__(name):
            pass
        else:
            self.page = Page(self)

            tekst = QtWidgets.QGraphicsTextItem("New text box")
            tekst.setFlag(QtWidgets.QGraphicsTextItem.ItemIsMovable)
            tekst.setFlag(QtWidgets.QGraphicsTextItem.ItemIsFocusable)
            tekst.setFlag(QtWidgets.QGraphicsTextItem.ItemIsSelectable)
            tekst.setTextInteractionFlags(QtCore.Qt.TextEditorInteraction)
            tekst.setFont("Sanserif")
            tekst.setScale(1)
            tekst.setPlainText(txt)
            tekst.setZValue(-4)

            
            
            self.page.scene.addItem(tekst)

            self.addTab(self.page, name)
            self.index = self.indexOf(self.page)
            print(self.index)
            self.tabNames.append(name)
            self.setCurrentIndex(self.count() - 1)
            print(self.tabNames)
            self.parent.parent.actions_dict["add_new_text_box"].triggered.connect(self.page.new_text_box)





    def newFile(self, path):

        if path:
            with open(path, "w", encoding="utf8") as f:
                f.write("")
                f.close()
                if self.parent.currentWidget() is not None:
                    self.open_f(path)

    def open_f(self, path):


            with open(path, encoding="utf8") as f:
                self.txt = f.read()
                # if self is not None : 
                self.dodaj(path.split("/")[-1], self.txt)
                    # self.path = ""
                self.path = path
                    
                self.parent.parent.status_bar.showMessage("Loaded file: " + path.split("/")[-1])

    def delete_f(self, path):
        nam = path.split("/")[-1]
        ind = self.tabNames.index(nam)

        self.removeTab(ind)
        self.tabNames.pop(ind)
        self.f = QtCore.QFile(path)
        self.f.remove()

            
