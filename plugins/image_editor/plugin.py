from plugin_framework.extension import Extension
from .widget import ImageEdit

class Plugin(Extension):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)
        self.widget = ImageEdit(iface.central_widget)
        self.widget.work_type = "Slika"
        self.parent = self.iface.central_widget
        #self.index = self.widget.currentIndex()
        self.active = False
        
        
        print("INIT TEST")

    # FIXME: implementacija apstraktnih metoda
    def activate(self):
        #print("Activated ImageEdit")
        
        self.active = True
        self.iface.central_widget.add_widget(self.widget)
        
        #self.iface.status_bar.showMessage("Activated " + self.widget.name)
        
        

    def deactivate(self):
        #print("Deactivated ImageEdit")
        self.active = False
        self.widget.clear()
        self.iface.central_widget.delete_tab()
        