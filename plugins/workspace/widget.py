from asyncio.windows_events import NULL
import os
import sys
sys.path.append("..")
from PySide2.QtGui import QActionEvent, QWindow, Qt
from ui.dialogs.new_collection import AnotherWindow
from ui.dialogs.new_file import NewFile
from PySide2 import QtWidgets, QtCore


class StructureDock(QtWidgets.QDockWidget):
    widget_for = 222222222
    klik = QtCore.Signal(str)
    double_klik = QtCore.Signal(str)
    def __init__(self, title, parent):
        super().__init__(title, parent)
        self.parent = NULL
        self.aw = AnotherWindow()
        self.nf = NewFile()
        
        self.model = QtWidgets.QFileSystemModel()
        self.model.setRootPath(QtCore.QDir.rootPath())
        
        
   
        self.tree = QtWidgets.QTreeView()
        self.tree.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.tree.customContextMenuRequested.connect(self.openMenu)
        self.tree.setModel(self.model)
        self.tree.viewport().installEventFilter(self)
        self.tree.setRootIndex(self.model.index(".\docs"))
        self.tree.clicked.connect(self.doc_clicked)
        self.tree.doubleClicked.connect(self.doc_double_clicked)
        # self.tree.installEventFilter(self)
        # self.tree.viewport().installEventFilter(self)

        self.setWidget(self.tree)
        

        self.setWindowTitle("Collections")
        self.setAllowedAreas(QtCore.Qt.LeftDockWidgetArea)
        self.klik.connect(self.pr)
        # self.klik.connect(self.openMenu)

        
    def doc_clicked(self, index):
        print(self.model.filePath(index))
        path = self.model.filePath(index)
        
        self.klik.emit(path)

    def doc_double_clicked(self, index):
        print(self.model.filePath(index))
        path = self.model.filePath(index)
        self.double_klik.emit(path)

    def pr(self, path):
        # self.aw = AnotherWindow()
        # self.aw = AnotherWindow()
        # self.aw.show()

        # if path:
            # print("IS not NONE")
            # print(str(path))
            self.aw.path = path
            self.nf.path = path
            # return str(path)
            # self.aw.path = "./docs"
        # else:
            
            # self.aw.path = ".\docs"
        # print(path)
        

        # return self.aw.path

    def new(self):
        
        
        self.aw.show()
        self.pr
    def newf(self):
            
        
        self.nf.show()
        self.pr
       

    def eventFilter(self, obj, event):
        if (
            obj is self.tree.viewport()
            and event.type() == QtCore.QEvent.MouseButtonPress
        ):
            ix = self.tree.indexAt(event.pos())
            if not ix.isValid():
                # print("empty area")
                self.tree.clearSelection()
                self.aw.path = ".\docs"
                self.nf.path = ".\docs"
        # if event.type() == QtCore.QEvent.ContextMenu:
        #     menu = QtWidgets.QMenu()
        #     menu.addAction('Action 1')
        #     menu.addAction('Action 2')
        #     menu.addAction('Action 3')

        #     if menu.exec_(event.globalPos()):
        #         item = source.itemAt(event.pos())
        #         print(item.text())
        #     return True
        return super(StructureDock, self).eventFilter(obj, event)

    def remove_item(self):
        # selected_item = self.aw.path  
        # for selected_item in selected_items:          
            # self.tree.delete(selected_item)
        # print(selected_item)
        # os.removedirs(selected_item)
        # os.rename(selected_item, selected_item)
        # self.change_name("novi")
        index = self.tree.currentIndex()
        file_path = self.model.filePath(index)
        self.dir = QtCore.QDir(file_path)
        self.f = QtCore.QFile(file_path)
        qm = QtWidgets.QMessageBox()
        rez = qm.question(self,'', "Da li sigurno zelite obrisati?", qm.Yes | qm.No)

        if rez == qm.Yes:
            # self.price_box.setText("0")
            # self.results_tax.setText("")
            # self.results_window.setText("")
            # self.tax_rate.setValue(21)
            self.dir.removeRecursively()
            self.parent.currentWidget().delete_f(file_path)
            
        else:
            pass
            # self.dir.removeRecursively()
        # os.startfile(file_path)
        # self.dir.
        # os.removedirs(file_path)
        # print(file_path)

    def openMenu(self, position):
      
        # indexes = self.tree.selectedIndexes()
        # if len(indexes) > 0:
         
        #     level = 0
        #     index = indexes[0]
        #     while index.parent().isValid():
        #         index = index.parent()
        #         level += 1
         
        menu = QtWidgets.QMenu()
        # if level == 0:
        
        self.act = QtWidgets.QAction('Delete')
        
        menu.addAction(self.act)
        
        
        # elif level == 1:
        #      menu.addAction('Action 1')
        # elif level == 2:
        #      menu.addAction('Action 1')
        # self.act.triggered.connect(self.remove_item)
        action = menu.exec_(self.tree.viewport().mapToGlobal(position))
        if action == self.act:
            self.remove_item()
        
        
  
        
            
 
