
from PySide2 import QtCore, QtWidgets
from plugin_framework.extension import Extension

from .widget import StructureDock


class Plugin(Extension):
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)
        
  

        self.sd = (QtWidgets.QDockWidget)

        self.widget = StructureDock("title", None)
        self.widget.parent = self.iface.central_widget

        self.widget.work_type = "workspace"
        self.parent = self.iface

        
        # self.widget.double_klik.connect(self.iface.open_f) 
        # if self.iface.central_widget.currentWidget() == "handler":
        #     self.v  = self.iface.central_widget.currentWidget().newFile()
        #     self.iface.actions_dict["new_file"].triggered.connect(self.widget.newf(self.v))
        # else:
        #     self.widget.double_klik.connect(self.d.open_f)  

        # self.widget.klik.connect(self.widget.pr) 
        self.iface.actions_dict["new_collection"].triggered.connect(self.widget.new)
        # self.iface.actions_dict["new_file"].triggered.connect(self.widget.newf)
        self.active = False 

        
        print("INIT TEST")


    def activate(self):

        
        self.active = True

        self.parent.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.widget)
        self.widget.show()
        self.widget.nf.item = self.iface.central_widget.currentWidget()

        self.widget.double_klik.connect(self.iface.central_widget.currentWidget().open_f)
        self.iface.actions_dict["new_file"].triggered.connect(self.widget.newf)

        



        
        

    def deactivate(self):
        # print("Deactivated Workspace")
        self.active = False
        # self.widget.clear()
        self.widget.hide()
        # self.iface.central_widget.delete_tab()
        # self.iface.removeDockWidget()